# lol_champion_detail

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Project demo with Gitlab pages
See [Demo of this project.](https://damorz.gitlab.io/lol_monitor_assist).
