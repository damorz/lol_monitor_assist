import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/form',
    name: 'Form',
    component: () => import('../views/Form.vue')
  },
  {
    path: '/detail',
    name: 'Detail',
    component: () => import('../views/Detail.vue')
  },
  {
    path: '/howtouse',
    name: 'HowToUse',
    component: () => import('../views/HowToUse.vue')
  },
  {
    path: '/vs',
    name: 'Vs',
    component: () => import('../views/Vs.vue')
  },
  {
    path: '/winner',
    name: 'Winner',
    component: () => import('../views/Winner.vue'),
    props: true
  },
  {
    path: '/loading',
    name: 'Loading',
    component: () => import('../views/Loading.vue')
  },
  {
    path: '*',
    name: 'Error',
    component: () => import('../views/Error.vue')
  }
]

const router = new VueRouter({
  // mode: 'history',
  routes
})

export default router
