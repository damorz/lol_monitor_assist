import Vue from "vue";
import Vuex from "vuex";
import App from "./App.vue";
import router from "./router";
import vueDebounce from "vue-debounce";
import store from "./store";
import Buefy from "buefy";
import "./assets/scss/app.scss";

Vue.use(Buefy);

Vue.config.productionTip = false;

Vue.use(Buefy);
Vue.use(Vuex);
Vue.use(vueDebounce, {
  listenTo: "input",
  defaultTime: "700ms",
});

new Vue({
  render: (h) => h(App),
  router,
  store,
}).$mount("#app");
