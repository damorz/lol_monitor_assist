import axios from "axios";
import championFormatter from "../../helper/championFormatter"
export default {
  namespaced: true,
  state: {
    championsName: [],
    dataTeam1: [],
    dataTeam2: [],
    gameVersion: '',
    gameInfo: ""
  },
  mutations: {
    SET_GAME_INFO(state, data) {
      state.gameInfo = data;
    },
    SET_GAME_VERSION(state, data) {
      state.gameVersion = data;
    },
    SET_DATA_TEAM_1(state, params) {
      state.dataTeam1[params.index] = params.data;
    },
    SET_DATA_TEAM_2(state, params) {
      state.dataTeam2[params.index] = params.data;
    },
    SET_CHAMPIONS_NAME(state, data) {
      state.championsName = data;
    },
  },
  getters: {
    getGameInfo: (state) => state.gameInfo,
    getGameVersion: (state) => state.gameVersion,
    getChampionsName: (state) => state.championsName,
    getDataTeam1: (state) => state.dataTeam1,
    getDataTeam2: (state) => state.dataTeam2,
  },
  actions: {
    setGameInfo({ commit }, data) {
      if (data) {
        commit("SET_GAME_INFO", data);
      }
    },
    addToCompleteData({ commit }, data) {
      let params = {
        index: 0,
        data: data,
      }

      params.index = data.position;
      console.log(data);
      if (data.teamNo === 1) {
        commit("SET_DATA_TEAM_1", params);
      } else {
        commit("SET_DATA_TEAM_2", params);
      }
    },
    async loadGameVersion({ commit }) {
      const version_url = `https://ddragon.leagueoflegends.com/api/versions.json`;
      try {
        const response = await axios.get(version_url);
        commit("SET_GAME_VERSION", response.data[0]);
      } catch (error) {
        console.error("Load game verison failed.", error);
      }
    },
    async loadChampionsName({ commit }) {
      const all_champions_api_url = `https://ddragon.leagueoflegends.com/cdn/${this.getters['champion/getGameVersion']}/data/en_US/champion.json`;
      try {
        const response = await axios.get(all_champions_api_url);
        commit("SET_CHAMPIONS_NAME", Object.keys(response.data.data));
      } catch (error) {
        console.error("Load all champion's name failed.", error);
      }
    },
    async loadChampionData(context, championName) {
      const champion_data_api_url = `https://ddragon.leagueoflegends.com/cdn/${this.getters['champion/getGameVersion']}/data/en_US/champion/${championName}.json`;
      try {
        const response = await axios.get(champion_data_api_url);
        const championData = championFormatter(response.data.data[championName])
        return championData;
      } catch (error) {
        console.error("Load champion data failed.", error);
      }
    },
  },
};
