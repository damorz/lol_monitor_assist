import Vuex from "vuex";
import Vue from "vue";
import champion from "./modules/champion"
Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
      champion,
    }
});
export default store;
