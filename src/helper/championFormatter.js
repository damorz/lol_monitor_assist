export default (champoinData) => {
    const championImage = champoinData.image.full;
    const essentialData = {
        name: champoinData.name,
        lore: champoinData.lore,
        skills: {
            q: {
                name: champoinData.spells[0].name,
                description: champoinData.spells[0].description,
                cooldown: champoinData.spells[0].cooldownBurn,
                cost: champoinData.spells[0].costBurn,
                range: champoinData.spells[0].rangeBurn,
            },
            w: {
                name: champoinData.spells[1].name,
                description: champoinData.spells[1].description,
                cooldown: champoinData.spells[1].cooldownBurn,
                cost: champoinData.spells[1].costBurn,
                range: champoinData.spells[1].rangeBurn,
            },
            e: {
                name: champoinData.spells[2].name,
                description: champoinData.spells[2].description,
                cooldown: champoinData.spells[2].cooldownBurn,
                cost: champoinData.spells[2].costBurn,
                range: champoinData.spells[2].rangeBurn,
            },
            r: {
                name: champoinData.spells[3].name,
                description: champoinData.spells[3].description,
                cooldown: champoinData.spells[3].cooldownBurn,
                cost: champoinData.spells[3].costBurn,
                range: champoinData.spells[3].rangeBurn,
            },
            passive: {
                name: champoinData.passive.name,
                description: champoinData.passive.description,

            },
        },
        images: {
            small: `http://ddragon.leagueoflegends.com/cdn/11.1.1/img/champion/${championImage}`,
            big: `http://ddragon.leagueoflegends.com/cdn/img/champion/loading/${championImage}_0.jpg`,
            q: `http://ddragon.leagueoflegends.com/cdn/11.1.1/img/spell/${champoinData.spells[0].image.full}`,
            w: `http://ddragon.leagueoflegends.com/cdn/11.1.1/img/spell/${champoinData.spells[1].image.full}`,
            e: `http://ddragon.leagueoflegends.com/cdn/11.1.1/img/spell/${champoinData.spells[2].image.full}`,
            r: `http://ddragon.leagueoflegends.com/cdn/11.1.1/img/spell/${champoinData.spells[3].image.full}`,
            passive: `http://ddragon.leagueoflegends.com/cdn/11.1.1/img/passive/${champoinData.passive.image.full}`,
        }
    }
    return essentialData;
}