/* eslint-disable no-undef */
module.exports = {
    devServer: {
      open: process.platform === "darwin",
      host: "0.0.0.0",
      port: 8000,
      https: false,
      hotOnly: false,
    },
    publicPath: '',
  }